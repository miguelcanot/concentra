﻿using Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Api.Services
{
    public class PersonaService
    {
        ConcentrateEntities context = new ConcentrateEntities();

        /// <summary>
        /// Obtener listado de personas.
        /// </summary>
        /// <param name="busqueda"></param>
        /// <returns></returns>
        public IEnumerable<object> ObtenerListaPersona(string busqueda)
        {
            try
            {

                // Ejemplo con join, donde persona debe tener un registro con persona detalle.
                var listaPersona = (from p in context.Persona
                                    join pd in context.PersonaDetalle on p.IDPersona equals pd.IDPersona
                                    where p.Estatus == "A" && (p.Nombre.Contains(busqueda) || p.Apellido.Contains(busqueda) || busqueda == "")
                                    select new { p.Nombre, p.Apellido, p.FechaNacimiento, p.IDPersona, p.Estatus,
                                        Detalle = new { pd.Genero, pd.IDPersonaDetalle, pd.Telefono, pd.Celular, pd.Cedula, pd.Direccion, pd.Correo } }
                                    ).ToList();

                // Ejemplo con left join, donde persona no necesita tener registros en la tabla persona detalle.
                var listaPersonaLJ = (from p in context.Persona
                                    join pd in context.PersonaDetalle on p.IDPersona equals pd.IDPersona into pdt
                                    from pde in pdt.DefaultIfEmpty()
                                    where p.Estatus == "A" && (p.Nombre.Contains(busqueda) || p.Apellido.Contains(busqueda) || busqueda == "")
                                    select new
                                    {
                                        p.Nombre,
                                        p.Apellido,
                                        p.FechaNacimiento,
                                        p.IDPersona,
                                        p.Estatus,
                                        Detalle = new { pde.Genero, pde.IDPersonaDetalle, pde.Telefono, pde.Celular, pde.Cedula, pde.Direccion, pde.Correo }
                                    }
                                    ).ToList();
                // Usando el Join
                return listaPersona;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        /// <summary>
        /// Insertar persona nueva.
        /// </summary>
        /// <param name="persona"></param>
        /// <returns></returns>
        public int Insertar(Persona persona)
        {
            try
            {
                context.Persona.Add(persona);
                context.SaveChanges();
                return persona.IDPersona;
            }
            catch (Exception e)
            {
                return 0;
            }
        }

        /// <summary>
        /// Modificar persona.
        /// </summary>
        /// <param name="persona"></param>
        /// <returns></returns>
        public bool Modificar(Persona persona)
        {
            try
            {
                var personaTemp = (from p in context.Persona
                                   where p.IDPersona == persona.IDPersona
                                   select p).SingleOrDefault();
                // Validar que no este null
                if (personaTemp != null)
                {
                    personaTemp.Nombre = persona.Nombre;
                    personaTemp.Apellido = persona.Apellido;
                    personaTemp.FechaNacimiento = persona.FechaNacimiento;
                    context.SaveChanges();
                    return true;
                }
                //var nombre = personaTemp?.Nombre ?? "no tengo datos";
                //var apellido = (personaTemp != null) ? personaTemp.Nombre : "Nada";
            }
            catch (Exception e)
            {
                // registro en el log.   
            }
            return false;
        }

        /// <summary>
        /// Cambiar estatus por id.
        /// </summary>
        /// <param name="idPersona"></param>
        /// <param name="estatus"></param>
        /// <returns></returns>
        public bool CambiarEstatus(int idPersona, string estatus)
        {
            try
            {
                var personaTemp = (from p in context.Persona
                                   where p.IDPersona == idPersona
                                   select p).SingleOrDefault();
                // Validar que no este null
                if (personaTemp != null)
                {
                    personaTemp.Estatus = estatus;
                    context.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {
                // registro en el log.   
            }
            return false;
        }

        /// <summary>
        /// Obtener una persona por id.
        /// </summary>
        /// <param name="idPersona"></param>
        /// <returns></returns>
        public Persona ObtenerRegistroPorId(int idPersona)
        {
            try
            {
                var personaTemp = (from p in context.Persona
                                   where p.IDPersona == idPersona
                                   select p).SingleOrDefault();
                // Validar que no este null
                if (personaTemp != null)
                {
                    return personaTemp;
                }
            }
            catch (Exception e)
            {
                // registro en el log.   
            }
            return new Persona();
        }

        #region persona detalle
        /// <summary>
        /// Insertar detalle de la persona
        /// </summary>
        /// <param name="personaDetalle"></param>
        /// <returns></returns>
        public bool InsertarDetalle(PersonaDetalle personaDetalle)
        {
            try
            {
                context.PersonaDetalle.Add(personaDetalle);
                context.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        #endregion persona detalle

        
    }
}