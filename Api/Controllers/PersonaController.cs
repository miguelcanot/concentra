﻿using Api.Models;
using Api.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Api.Controllers
{
    public class PersonaController : ApiController
    {
        private PersonaService personaService = new PersonaService();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult ObtenerListado()
        {
            var busqueda = "";
            // Obtener listado de personas activas.
            var lista = personaService.ObtenerListaPersona(busqueda);
            // Devolver objeto json
            var resultado = new { estatus = true, mensaje = "", resultado = lista };
            return Ok(resultado);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public IHttpActionResult Insertar()
        {
            string mensaje = "";
            bool estatus = false;

            // Obtener el post enviado desde el cliente
            var request = HttpContext.Current.Request;
            
            // Llenar los datos de la persona
            var persona = new Persona();
            persona.Nombre = request["nombre"];
            persona.Apellido = request["apellido"];
            persona.Estatus = "A";
            // Insertar la persona nueva y obtener el id
            var idPersona = personaService.Insertar(persona);

            // Insertar el detalle de la persona
            personaService.InsertarDetalle(new PersonaDetalle { IDPersona = idPersona });
            
            if (idPersona == 0)
            {
                mensaje = "Error en el server";
            }
            else
            {
                estatus = true;
                mensaje = "Información guardada correctamente.";
            }
            // Devolver objeto json
            var resultado = new { estatus = estatus, mensaje = mensaje, resultado = "" };
            
            return Ok(resultado);
        }
    }
}
