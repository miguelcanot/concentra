﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Proyecto.Subclase;

namespace Proyecto
{
    /// <summary>
    /// 
    /// </summary>
    public class A : B, Implementacion
    {
        private string _nombre;
        public string Nombre
        {
            get
            {
                return this._nombre;
            }
            set
            {
                this._nombre = value;
            }
        }

        public string Valor
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public A(string nombre) : base(nombre)
        {
            this.Nombre = nombre;
        }

        public override string GetApellido()
        {
            base.GetApellido();
            return "";
        }

        public void RealizarImplementacion()
        {
            throw new NotImplementedException();
        }
    }
}
