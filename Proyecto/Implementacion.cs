﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto
{
    public interface Implementacion
    {
        string Valor { get; set; }

        void RealizarImplementacion();
    }
}
