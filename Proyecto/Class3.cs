﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto
{
    public abstract class Abstracta
    {
        public string Nombre { get; set; }

        public abstract void GetApellido();

        public string GetNombre()
        {
            return this.Nombre;
        }
    }

    public class PRUEBA : Abstracta
    {
        public PRUEBA()
        {
       
        }

        public override void GetApellido()
        {

        }
    }
}
