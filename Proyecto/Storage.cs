﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto
{
    public class Storage<T>
    {
        public List<T> Elementos { get; set; }

        public void Insertar(T elemento)
        {
            
            this.Elementos.Add(elemento);
        }

        public List<T> ObtenerLista()
        {
            return this.Elementos;
        }
    }

    public class Programa
    {
        public Programa()
        {
            var storage = new Storage<string>();
            storage.Insertar("Erickson");

            var productosStorage = new Storage<int>();
            productosStorage.Insertar(0);
        }
    }
}
