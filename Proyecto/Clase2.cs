﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Proyecto.Subclase
{
    public class B
    {
        public string Apellido { get; set; }

        public B(string apellido)
        {
            this.Apellido = apellido;
        }

        public virtual string GetApellido()
        {
            return "";
        }
    }
}
