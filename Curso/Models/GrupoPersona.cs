﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curso.Models
{
    public class GrupoPersona
    {
        public int IdPersona { get; set; }
        public int IdGrupo { get; set; }
    }
}
