﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curso.Models
{
    public class Result<T> where T : class
    {
        public int Estatus { get; set; }
        public string Mensaje { get; set; }
        public T Valor { get; set; }

        public Result()
        {
        }

        public Result(int estatus, string mensaje)
        {
            this.Estatus = estatus;
            this.Mensaje = mensaje;
        }
    }

    public class Result
    {
        public int Estatus { get; set; }
        public string Mensaje { get; set; }

        public Result()
        {
        }
        public Result(int estatus, string mensaje)
        {
            this.Estatus = estatus;
            this.Mensaje = mensaje;
        }
    }
}
