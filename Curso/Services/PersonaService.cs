﻿using Curso.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curso.Services
{
    public class PersonaService
    {
        private List<Persona> personas;
        private List<Grupo> grupos;
        private List<GrupoPersona> grupoPersonas;

        public PersonaService()
        {
            personas = new List<Persona>();
            grupos = new List<Grupo>();
            grupoPersonas = new List<GrupoPersona>();

            //Inicializar listados.
            personas.Add(new Persona()
            {
                Id = 1,
                Nombre = "Jefri",
                Apellido = "Martinez"
            });

            personas.Add(new Persona()
            {
                Id = 2,
                Nombre = "Michael",
                Apellido = "Buble"
            });

            //Grupos 
            grupos.Add(new Grupo()
            {
                Id = 1,
                Nombre = "Programadores"
            });

            grupos.Add(new Grupo()
            {
                Id = 2,
                Nombre = "Artistas"
            });

            //Grupos persona.
            grupoPersonas.Add(new GrupoPersona()
            {
                IdPersona = 1,
                IdGrupo = 1
            });

            grupoPersonas.Add(new GrupoPersona()
            {
                IdPersona = 2,
                IdGrupo = 2
            });
        }

        public Result InsertarPersona(Persona persona)
        {
            var result = new Result();
            try
            {
                personas.Add(persona);

                result.Estatus = 200;
                result.Mensaje = "Se ha insertado la persona correctamente";
            }
            catch (Exception ex)
            {
                result.Estatus = 500;
                result.Mensaje = ex.Message;
            }
            return result;
        }

        public Result InsertarPersonas(IEnumerable<Persona> lista)
        {
            var result = new Result();
            try
            {
                foreach (var item in lista)
                    personas.Add(item);

                result.Estatus = 200;
                result.Mensaje = "Se ha insertado el grupo de personas correctamente";
            }
            catch (Exception ex)
            {
                result.Estatus = 500;
                result.Mensaje = ex.Message;
            }
            return result;
        }

        public Result<List<Persona>> ObtenerLista(string busqueda)
        {

            /*
                Consultas utilizando linq 
                clausulas from variableONombre in (nombre de la colección que deseas manipular)
             */
            var resultado = from p in this.personas
                            where p.Apellido == "Martinez" && p.Nombre == "Jefri"
                            orderby p.Id descending
                            select new
                            {
                                Nombre = p.Nombre,
                                Apellido = p.Apellido
                            };

            // Se puede alterar los resultados y agregar propiedades nuevas si se requiere
            // Retornando una función anónima.
            var resultadoUpdated = (from p in this.personas
                                    where p.Apellido == "Martinez" && p.Nombre == "Jefri"
                                    orderby p.Id descending
                                    select new
                                    {
                                        Nombre = p.Nombre,
                                        Apellido = p.Apellido,
                                        NombreCompleto = p.Nombre + " " + p.Apellido
                                    }).SingleOrDefault();


            //Se puede alterar los resultados y agregar propiedades nuevas si se requiere 
            //retornando una función anónima utilizando joins uniendo dos entidades.
            var resultadoNuevo = from g in this.grupos
                                 join gp in this.grupoPersonas on g.Id equals gp.IdGrupo
                                 join p in this.personas on gp.IdPersona equals p.Id
                                 select new
                                 {
                                     NombreGrupo = g.Nombre,
                                     NombrePersona = p.Nombre
                                 };

            //join gp in this.grupoPersonas buscando en base a un parametro 
            var busquedaResult = from p in this.personas
                                 where p.Nombre.Contains(busqueda)
                                 orderby p.Id descending
                                 select new
                                 {
                                     Nombre = p.Nombre,
                                     Apellido = p.Apellido
                                 };

            //

            var result = new Result<List<Persona>>();
            result.Estatus = 200;
            result.Mensaje = "Devuelto";
            result.Valor = this.personas;

            return result;
        }
    }
}
