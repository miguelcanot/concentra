﻿using Curso.Models;
using Curso.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Curso
{
    class Program
    {
        static void Main(string[] args)
        {
            var personaService = new PersonaService();
            do
            {
                #region Seccion de pedir informacion de persona.

                Persona persona = new Persona();

                Console.WriteLine("Ingrese el nombre: ");
                persona.Nombre = Console.ReadLine();

                Console.WriteLine("Ingrese el apellido");
                persona.Apellido = Console.ReadLine();

                Console.WriteLine("Ingrese la edad");

                int edad = 0;
                string textoEdad = Console.ReadLine();
                int.TryParse(textoEdad, out edad);

                #endregion


                #region Guardado de personas.

                var result = personaService.InsertarPersona(persona);
                if (result.Estatus == 200)
                    Console.Write("Se insertó la persona correctamente");
                else
                    Console.WriteLine("Ha ocurrido un error");

                #endregion


                #region Consulta de personas

                var resultadoConsulta = personaService.ObtenerLista();
                foreach (var consulta in resultadoConsulta.Valor)
                {
                    Console.WriteLine($"Persona {consulta.Nombre} {consulta.Apellido} su edad es {consulta.Edad}");
                }

                Console.ReadKey();

                #endregion
            } while (true);
        }
    }
}
