﻿using LinQLambda.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace LinQLambda
{
    class Program
    {
        private static DBConcentrate context = new DBConcentrate();

        static void Main(string[] args)
        {

            var listaPersona = context.Persona.Select(x=>x.Nombre);

            var lista = context.Persona.Join(context.PersonaDetalle, p => p.IDPersona, pd => pd.IDPersona,
                (p, pd) => new { Per = p, PerD = pd });


            var persona = new Persona { Nombre = "Jaaaa", Apellido = "Mesa", Estatus = "A" };

            //var personaDetalle = new PersonaDetalle{ IDPersona = 1, IDPersonaDetalle = 1, Genero = "M", Correo = "Elcorreo@asd.asd" };

            //persona.personaDetalleD = personaDetalle;
  
            //foreach (var personaItem in listaPersona)
            //{
            //    var genero = personaItem.PersonaDetalle.FirstOrDefault().Genero;
            //}

            //// Crear objeto persona
            //var persona = new Persona { Nombre = "Jose" };
            //persona.Apellido = "Perez";
            //persona.Estatus = "A";

            //// Obtener fecha.
            //string fecha = "2000/10/20";
            //// Convertir fecha.
            //DateTime fechaC = Convert.ToDateTime(fecha);
            //// Asignar fecha a objeto persona.
            //persona.FechaNacimiento = fechaC;

            //// Insertar DB.
            //var idPersona = Insertar(persona);

            //// Mostrar listado de personas.
            //var listado = ObtenerListaPersona();
        }

        /// <summary>
        /// Insertar persona.
        /// </summary>
        /// <param name="persona"></param>
        static int Insertar(Persona persona)
        {
            context.Persona.Add(persona);
            context.SaveChanges();

            //var core = new Core<Persona>();
            //core.InsertarTemp(persona);

            return persona.IDPersona;
        }

        /// <summary>
        /// Modificar persona.
        /// </summary>
        /// <param name="persona"></param>
        /// <returns></returns>
        static bool Modificar(Persona persona, PersonaDetalle personaDetalle)
        {
            try
            {
                var personaTemp = (from p in context.Persona
                                   where p.IDPersona == persona.IDPersona
                                   select p).SingleOrDefault();
                if (personaTemp != null)
                {
                    personaTemp.Nombre = persona.Nombre;
                    personaTemp.Apellido = persona.Apellido;
                    personaTemp.FechaNacimiento = persona.FechaNacimiento;
                    context.SaveChanges();

                    // Modificar detalle.
                    var personaDetalleTemp = (from pd in context.PersonaDetalle
                                          where pd.IDPersona == personaDetalle.IDPersona
                                          select pd).SingleOrDefault();

                    if (personaDetalleTemp != null)
                    {
                        personaDetalle.Genero = personaDetalle.Genero;
                        context.SaveChanges();
                    }

                    return true;
                }
            }
            catch (Exception e)
            {
                // Registrar el error en el log de errores.
                
            }
            return false;

        }

        /// <summary>
        /// Obtener listado de personas.
        /// </summary>
        /// <returns></returns>
        static List<Persona> ObtenerListaPersona()
        {
            // Linq
            var listaPersona = (from p in context.Persona
                                select p).ToList();

            // Lambda
            //var listado = context.Persona.Join(,) .Join(x=>x.per)

            return listaPersona;
        }

        static Persona ObtenerPersonaPorId(int idPersona)
        {
            var persona = (from p in context.Persona
                           where p.IDPersona == idPersona
                           select p).SingleOrDefault();

            if (persona != null)
            {
                return persona;
            } 
            else
            {
                return new Persona();
            }
        }
    }
}
