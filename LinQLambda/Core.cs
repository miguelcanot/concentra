﻿using LinQLambda.models;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;

namespace LinQLambda
{
    class Core<T> where T : class, new()
    {
        private static DBConcentrate context = new DBConcentrate();

        public void InsertarTemp(T objeto)
        {
            DbEntityEntry dbEntityEntry = context.Entry(objeto);
            context.Set<T>().Add(objeto);
            context.SaveChanges();
        }
    }
}
