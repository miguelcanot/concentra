﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComentariosArray
{

    /// <summary>
    /// Extensions methods. 
    /// 
    /// Extensión de métodos.
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// Métodos extensibles que te permiten extender funcionalidad del de un número o 
        /// un tipo en particular para agregar una nueva funcionalidad.
        /// 
        /// Donde el primer parámetro será el elemento en cuestión y a partir de este iniciarán
        /// los parametros requeridos. Ver ejemplo más abajo con fecha y con entero.
        /// </summary>
        /// <param name="numero"></param>
        /// <param name="numeroComparado"></param>
        /// <returns></returns>
        public static bool EsMayorQue(this int numero, int numeroComparado)
        {
            if (numero > numeroComparado)
                return true;
            else
                return false;
        }

        public static bool EstaEntre(this DateTime fecha, DateTime fechaInicio, DateTime fechaFin)
        {
            if (fecha >= fechaInicio && fecha <= fechaFin)
                return true;
            else
                return false;
        }
    }

    class Program
    {
        //Propfull para generar la propiedad completa.
        private int numero;
        public int Numero
        {
            get { return numero; }
            set { numero = value; }
        }

        static void Main(string[] args)
        {
            //Array.
            string[,,] array = new string[3, 2, 2];

            CSharpCurso curso = new CSharpCurso("Jefri");
            curso.GetNombre();

            int edad = 20;
            edad.EsMayorQue(29);

            // Consume más tiempo ya que debe evaluar cada condición
            // a cumplir.
            if (array.Length > 2)
            {

            }
            else if (array.Length == 4)
            {

            }

            //Va a la condición directamente.
            switch (array.Length)
            {
                case 1:
                    break;
            }

            //Operador ternario.
            int cantidad = (array != null) ? array.Length : 0;

            //Operador nulable.
            string nombre = "";
            var valor = nombre ?? "";

            //Ejemplo de uso:
            List<string> listado = new List<string>();
            listado.Add("Nombre");
            listado.Add("Apellido");

            var listadoFiltrado = listado.Where(x => x == "Ramon").ToList() ?? new List<string>();


            //Permite asignación de null a variables que no permiten valores como por ejemplo
            // int , DateTime entre otros. Ejemplo int?
            DateTime? fechaUno = null;
            DateTime fecha = (fechaUno.HasValue) ? fechaUno.Value : DateTime.Now;

            //Si es nulo retorna nulo, sino entonces punto. Elvis Operator.
            var nombreCurso = curso?.Nombre;


            // Estructuras de repetición.
            foreach (var item in listadoFiltrado)
            {
                Console.WriteLine(item);
            }

            //Foreach con Lambda expresion.
            listadoFiltrado.ForEach((item) =>
            {
                Console.WriteLine(item);
            });

            //Do While.
            do
            {
                //Accion a realizar.

            } while (true); //Loop infinito.

            //While.
            while (true)
            {
                //Realizar operación.
            }

            Nullable<int> elementa;

            //Count si son collections. Si no encuentra ahí puede una función
            // Count() y el Count property.
            //For.
            for (int i = 0; i < listadoFiltrado.Count(); i++)
            {


            }


            //Fecha
            DateTime fechaActual = DateTime.Now;
            DateTime fechaInicio = DateTime.Now;
            DateTime fechaFin = DateTime.Now;

            fechaActual.EstaEntre(fechaInicio, fechaFin);
        }
    }
}
