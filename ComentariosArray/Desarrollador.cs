﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComentariosArray
{
    public class DesarrolladorAttribute : Attribute
    {
        public string Nombre { get; set; }
        public string Nivel { get; set; }
        public bool Revisado { get; set; }

        public DesarrolladorAttribute(string nombre, string nivel, bool revisado)
        {
            this.Nombre = nombre;
            this.Nivel = nivel;
            this.Revisado = revisado;
        }
    }
}
