﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ComentariosArray
{
    /// <summary>
    ///  Esta es la clase del curso.
    /// </summary>
    /// 
    [Desarrollador("Jefri Martinez", "Senior", true)]
    public class CSharpCurso
    {

        /// <summary>
        ///  Esta es una propiedad de la clase curso el nombre del curso.
        /// </summary>
        public string Nombre { get; set; }

        /// <summary>
        ///  Un constructor
        /// </summary>
        /// <param name="nombre">Es el nombre del parametro</param>
        public CSharpCurso(string nombre)
        {
            this.Nombre = nombre;
        }


        /// <summary>
        /// Este método es utilizado para obtener el nombre.
        /// </summary>
        /// <returns>Retorna un string</returns>
        public string GetNombre()
        {
            return this.Nombre;
        }
    }
}
