
GO
/****** Object:  Table [dbo].[Log]    Script Date: 5/4/2017 3:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Log](
	[IDLog] [int] IDENTITY(1,1) NOT NULL,
	[IDUsuario] [int] NULL,
	[IDObjeto] [int] NULL,
	[IDRol] [int] NULL,
	[Fecha] [datetime] NULL,
	[Comentario] [varchar](1000) NULL,
 CONSTRAINT [PK_Log] PRIMARY KEY CLUSTERED 
(
	[IDLog] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Objeto]    Script Date: 5/4/2017 3:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Objeto](
	[IDObjeto] [int] IDENTITY(1,1) NOT NULL,
	[NombreLogico] [varchar](200) NULL,
	[NombreFisico] [varchar](300) NULL,
	[TipoObjeto] [varchar](10) NULL,
	[IDObjetoRelacionado] [int] NULL,
	[Estatus] [varchar](2) NULL,
 CONSTRAINT [PK_Objetos] PRIMARY KEY CLUSTERED 
(
	[IDObjeto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Rol]    Script Date: 5/4/2017 3:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Rol](
	[IDRol] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Descripcion] [varchar](500) NULL,
	[Estatus] [varchar](2) NOT NULL,
	[Url] [varchar](50) NULL,
 CONSTRAINT [PK_Rol] PRIMARY KEY CLUSTERED 
(
	[IDRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RolObjeto]    Script Date: 5/4/2017 3:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[RolObjeto](
	[IDRol] [int] NOT NULL,
	[IDObjeto] [int] NOT NULL,
	[RegitrarLog] [bit] NULL,
 CONSTRAINT [PK_Rol_Objeto] PRIMARY KEY CLUSTERED 
(
	[IDRol] ASC,
	[IDObjeto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 5/4/2017 3:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario](
	[IDUsuario] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](100) NOT NULL,
	[Contrasena] [varchar](100) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Correo] [varchar](100) NULL,
	[Telefono] [varchar](50) NULL,
	[Idioma] [varchar](10) NULL,
	[Estatus] [varchar](2) NOT NULL,
	[Token] [varchar](300) NULL,
	[FechaCambioClave] [datetime] NULL,
	[Fecha] [datetime] NOT NULL,
	[Salt] [varchar](100) NOT NULL,
	[Intento] [int] NOT NULL,
	[FechaBloqueo] [datetime] NULL,
	[IDEmpresa] [int] NULL,
	[Imagen] [varchar](100) NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY CLUSTERED 
(
	[IDUsuario] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UsuarioRol]    Script Date: 5/4/2017 3:59:53 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UsuarioRol](
	[IDUsuario] [int] NOT NULL,
	[IDRol] [int] NOT NULL,
	[Estatus] [varchar](2) NOT NULL,
 CONSTRAINT [PK_UsuarioRol] PRIMARY KEY CLUSTERED 
(
	[IDUsuario] ASC,
	[IDRol] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [DF_Usuario_EsChofer]  DEFAULT ((0)) FOR [Fecha]
GO
ALTER TABLE [dbo].[Usuario] ADD  CONSTRAINT [DF_Usuario_Intento]  DEFAULT ((0)) FOR [Intento]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Objeto] FOREIGN KEY([IDObjeto])
REFERENCES [dbo].[Objeto] ([IDObjeto])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Objeto]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Rol]
GO
ALTER TABLE [dbo].[Log]  WITH CHECK ADD  CONSTRAINT [FK_Log_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[Log] CHECK CONSTRAINT [FK_Log_Usuario]
GO
ALTER TABLE [dbo].[Objeto]  WITH CHECK ADD  CONSTRAINT [FK_Objetos_Objetos] FOREIGN KEY([IDObjetoRelacionado])
REFERENCES [dbo].[Objeto] ([IDObjeto])
GO
ALTER TABLE [dbo].[Objeto] CHECK CONSTRAINT [FK_Objetos_Objetos]
GO
ALTER TABLE [dbo].[RolObjeto]  WITH CHECK ADD  CONSTRAINT [FK_Rol_Objeto_Objeto] FOREIGN KEY([IDObjeto])
REFERENCES [dbo].[Objeto] ([IDObjeto])
GO
ALTER TABLE [dbo].[RolObjeto] CHECK CONSTRAINT [FK_Rol_Objeto_Objeto]
GO
ALTER TABLE [dbo].[RolObjeto]  WITH CHECK ADD  CONSTRAINT [FK_RolObjeto_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[RolObjeto] CHECK CONSTRAINT [FK_RolObjeto_Rol]
GO
ALTER TABLE [dbo].[Usuario]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Empresa] FOREIGN KEY([IDEmpresa])
REFERENCES [dbo].[Empresa] ([IDEmpresa])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK_Usuario_Empresa]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol_Rol] FOREIGN KEY([IDRol])
REFERENCES [dbo].[Rol] ([IDRol])
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_Usuario_Rol_Rol]
GO
ALTER TABLE [dbo].[UsuarioRol]  WITH CHECK ADD  CONSTRAINT [FK_Usuario_Rol_Usuario] FOREIGN KEY([IDUsuario])
REFERENCES [dbo].[Usuario] ([IDUsuario])
GO
ALTER TABLE [dbo].[UsuarioRol] CHECK CONSTRAINT [FK_Usuario_Rol_Usuario]
GO
